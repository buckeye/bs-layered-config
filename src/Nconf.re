open Relude.Globals;

type t;

[@bs.module] external nconf: t = "nconf";

module Format = {
  type t;

  [@bs.module] external toml: t = "nconf-toml";
};

[@bs.module "nconf"] external formats: Js.Dict.t(Format.t) = "formats";

// Initialize the TOML format.
Js.Dict.set(formats, "toml", Format.toml);

[@bs.send] external argv: t => t = "argv";

[@bs.send] external env: (t, string) => t = "env";

[@bs.send] external get: t => Js.Json.t = "get";

module FileOptions = {
  [@bs.deriving abstract]
  type t = {
    file: string,
    format: Format.t,
  };
};

[@bs.send] external file: (t, string, FileOptions.t) => unit = "file";

let file = (~format=Format.toml, ~path, ~name, t) => {
  let _ = FileOptions.t(~file=path, ~format) |> file(t, name);
  t;
};
