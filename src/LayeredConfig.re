open Relude.Globals;

let debug = Debug.make("layered-config");

exception BasePathNotAbsolute(string);
exception UnknownEnvironment(string);
exception MissingRequiredConfig(string);

module BasePath = {
  let resolve = (baseDir, filename) =>
    Node.Path.isAbsolute(baseDir)
      ? Node.Path.resolve(baseDir, filename)
      : raise(BasePathNotAbsolute(baseDir));
};

let addFiles = (baseDir, fileNames, t) => {
  let reducer = (filename, t) => {
    let name = Node.Path.basename(filename);
    let path = BasePath.resolve(baseDir, filename);

    Debug.logf2(debug, "Adding %s file at %s", (name, path));

    Nconf.file(~path, ~name, t);
  };

  Array.foldRight(reducer, t, fileNames);
};

let get = (nconf, key) =>
  Nconf.get(nconf) |> SelectN.get(key) |> Js.Null_undefined.toOption;

let unsafeGet = (nconf, key) =>
  switch (get(nconf, key)) {
  | Some(value) => value
  | None => raise(MissingRequiredConfig(key))
  };

let getOr = (nconf, key, otherwise) =>
  get(nconf, key) |> Option.getOrElse(otherwise);

let getIntWithWarning = (nconf, key) => {
  let json = get(nconf, key);

  let parse = json =>
    switch (json |> Js.Json.classify) {
    | JSONString(string) => string |> Int.fromString
    | JSONNumber(float) => float |> Int.fromFloat |> Option.pure
    | _ => None
    };

  let warnIfEmpty = parsed =>
    switch (parsed) {
    | Some(int) => Option.pure(int)
    | None =>
      Js.Console.warn({j|$key setting was not an integer: $json|j});
      None;
    };

  json |> Option.flatMap(parse) |> warnIfEmpty;
};

let map = (nconf, key, fn) => get(nconf, key) |> Option.map(fn);

let flatMap = (nconf, key, fn) => get(nconf, key) |> Option.flatMap(fn);

let isDev = nconf => {
  let env =
    get(nconf, "NODE_ENV")
    |> Option.flatMap(RJs.Json.toString)
    |> Option.map(String.toUpperCase);

  switch (env) {
  | Some("PRODUCTION") => false
  | Some("DEVELOPMENT") => true
  | Some("TEST") => true
  | Some(other) => raise(UnknownEnvironment(other))
  | None =>
    Js.Console.warn(
      {|
      ***`NODE_ENV` not set, defaulting to `DEVELOPMENT`***
      You should really consider setting the `NODE_ENV` environment variable.
    |},
    );
    true;
  };
};

let getVPNOffset = nconf =>
  getIntWithWarning(nconf, "VPN")
  |> Option.map(int => {
       Debug.logf(debug, "VPN offset is set to %s", int);
       int;
     });

let applyOffset = (nconf, default, key, value) => {
  let skip = "Skipping VPN offset application, environment is not DEV";
  let notDefault = key => {j|$key was overridden in the configuration, not applying offset|j};
  let noVPN = "VPN environment variable not available, skipping offset";

  switch (nconf->isDev, default == value, nconf->getVPNOffset) {
  | (false, _, _) => Debug.tapf(debug, skip, value)
  | (_, false, _) => Debug.tapf(debug, notDefault(key), value)
  | (_, _, None) => Debug.tapf(debug, noVPN, value)
  | (true, true, Some(offset)) => value + offset
  };
};

let getWithOffset = (nconf, key, default) =>
  getIntWithWarning(nconf, key)
  |> Option.map(applyOffset(nconf, default, key));

let unsafeGetWithOffset = (nconf, key, default) =>
  switch (getWithOffset(nconf, key, default)) {
  | Some(int) => int
  | None => raise(MissingRequiredConfig(key))
  };

let make = (~files=[|"defaults.toml", "environment.toml"|], configPath) => {
  Nconf.nconf
  |> Nconf.argv
  |> (t => Nconf.env(t, "__"))
  |> addFiles(configPath, files);
};
