let applyOffset: (Nconf.t, int, string, int) => int;

let get: (Nconf.t, string) => option(Js.Json.t);

let unsafeGet: (Nconf.t, string) => Js.Json.t;

let getOr: (Nconf.t, string, Js.Json.t) => Js.Json.t;

let isDev: Nconf.t => bool;

let map: (Nconf.t, string, Js.Json.t => 'b) => option('b);

let flatMap: (Nconf.t, string, Js.Json.t => option('b)) => option('b);

let getWithOffset: (Nconf.t, string, int) => option(int);

let unsafeGetWithOffset: (Nconf.t, string, int) => int;

let make: (~files: array(string)=?, string) => Nconf.t;
