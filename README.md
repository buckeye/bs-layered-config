# bs-layered-config

A layered configuration approach for a server side BuckleScript (JS) applications

### Parsing Caveat

You need to be careful parsing values which may come from a file or environment
variable or command line argument. All command line arguments and environment
variables will come into the system as strings.

### Usage

See the tests
