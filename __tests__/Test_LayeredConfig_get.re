open Relude.Globals;

let dirname = Option.getOrThrow([%bs.node __dirname]);
let configPath = Node.Path.resolve(dirname, "../config");

Ava.test("`get` for a setting with a value in defaults.toml", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.get(config, "NAMESPACE.setting")
  |> Option.getOrElseLazy(_ => RJs.Json.fromString("setting was missing"))
  |> RJs.Json.toString
  |> Option.getOrElseLazy(_ => "setting was not parsable as a string")
  |> Ava.Assert.is(t, "thing");
});

Ava.test("`get` for a setting with a value in environment.toml", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.get(config, "OVERRIDE.thing")
  |> Option.getOrElseLazy(_ => RJs.Json.fromString("setting was missing"))
  |> RJs.Json.toString
  |> Option.getOrElseLazy(_ => "setting was not parsable as a string")
  |> Ava.Assert.is(t, "should be this");
});

Ava.test("`unsafeGet`", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.unsafeGet(config, "NAMESPACE.setting")
  |> RJs.Json.toString
  |> Option.getOrElseLazy(_ => "setting was not parsable as a string")
  |> Ava.Assert.is(t, "thing");
});

Ava.test("`getOr` for a setting that does not exist", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.getOr(config, "DOES_NOT_EXIST.thing", RJs.Json.fromInt(42))
  |> RJs.Json.toInt
  |> Option.getOrElseLazy(_ => 0)
  |> Ava.Assert.is(t, 42);
});

Ava.test("`getOr` for a setting that exists", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.getOr(config, "DATABASE.port", RJs.Json.fromInt(42424))
  |> RJs.Json.toInt
  |> Option.getOrElseLazy(_ => 0)
  |> Ava.Assert.is(t, 12345);
});

Ava.test("`getWithOffset`", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.getWithOffset(config, "DATABASE.port", 12345)
  |> Option.getOrElseLazy(_ => 0)
  |> Ava.Assert.is(t, 12555);
});

Ava.test("`getWithOffset` missing config setting", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.getWithOffset(config, "DOES_NOT_EXIST.thing", 12345)
  |> Option.getOrElseLazy(_ => 42)
  |> Ava.Assert.is(t, 42);
});

Ava.test("`getWithOffset` overridden default", t => {
  let config = LayeredConfig.make(configPath);

  // Because our default is different than the set value in the configuration
  // file, the module should refuse to apply the offset.  Applying the offset
  // to an overridden value causes confusion that is difficult to debug.
  LayeredConfig.getWithOffset(config, "DATABASE.port", 67890)
  |> Option.getOrElseLazy(_ => 0)
  |> Ava.Assert.is(t, 12345);
});

Ava.test("`unsafeGetWithOffset`", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.unsafeGetWithOffset(config, "DATABASE.port", 12345)
  |> Ava.Assert.is(t, 12555);
});

Ava.test("`map`", t => {
  let config = LayeredConfig.make(configPath);

  let fn = json => json |> RJs.Json.toInt |> Option.getOrElseLazy(_ => (-1));

  LayeredConfig.map(config, "OVERRIDE.answer", fn)
  |> Option.getOrElseLazy(_ => (-2))
  |> Ava.Assert.is(t, 42);
});

Ava.test("`flatMap`", t => {
  let config = LayeredConfig.make(configPath);

  LayeredConfig.flatMap(config, "OVERRIDE.answer", RJs.Json.toInt)
  |> Option.getOrElseLazy(_ => (-1))
  |> Ava.Assert.is(t, 42);
});
